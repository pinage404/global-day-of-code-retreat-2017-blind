import chai from 'chai'
chai.should()

function nextGeneration(grid) {
	if (grid.length === 2) {
		return [
			[0],
			[0]
		]
	}
	if (grid.length === 1) {
		return [grid[0].map(element => 0) ];
	}
}


describe('Game of life', () => {
	it('cells with no neighbours', () => {
		const grid = [
			[1,],
		]
		nextGeneration(grid).should.be.deep.equal([
			[0,],
		])
	});

	describe('cells with one neighbour should die', () => {
		it('simplest horizontal', () => {
			const grid = [
				[1, 1,],
			]
			nextGeneration(grid).should.be.deep.equal([
				[0, 0,],
			])
		});

		it('horizontal', () => {
			const grid = [
				[1, 0, 1,],
			]
			nextGeneration(grid).should.be.deep.equal([
				[0, 0, 0,],
			])
		});

		it('simplest vertical', () => {
			const grid = [
				[1],
				[1],
			]
			nextGeneration(grid).should.be.deep.equal([
				[0],
				[0],
			])
		});

	});

});
